package com.db.mocking;

/**
 * @author Nilesh
 * @since 1.0
 */
public interface IBank {
    String deposit(double amount, String accountid);
    String withdraw(double amount, String accountid);
}
