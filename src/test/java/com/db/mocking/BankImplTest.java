package com.db.mocking;

import org.junit.*;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class BankImplTest {

    IBank bank = null;

    IBankService service = null;

    @BeforeClass
    public static void superInit() {
        System.out.println("Gets called only once before all the test");
    }

    @AfterClass
    public static void superDestroy() {
        System.out.println("gets called only once after all the test ...");
    }

    /**
     * THis method gets called before every test Method
     **/
    @Before
    public void init() {
        /**
         * Even if parag has not delivered his part i know he will deliver the same for... THe given Contract
         * if it is successfull he will return SUCCESS and IF not throw a Exception...
         * */
        service = Mockito.mock(IBankService.class); // this is mocking the bank object proxy creation code
        when(service.transaction(TransactionType.DEBIT, "AC001", 100)).thenReturn("SUCCESS");
        when(service.transaction(TransactionType.CREDIT, "AC001", 100)).thenReturn("SUCCESS");
        System.out.println("******************************");
        System.out.println(service.getClass().getName());

        bank = new BankImpl(service);
    }

    @Test
    public void testDeposit() {
        String actual = bank.deposit(100, "AC001");
        String expected = "SUCCESS";
        assertEquals(expected, actual);
    }

    @Test
    public void testWithdraw() {
        String actual = bank.withdraw(100, "AC001");
        String expected = "SUCCESS";
        assertEquals(expected, actual);
    }

    /**
     * After every test... Cleanup
     */
    @After
    public void destroy() {
        System.out.println("Gets called after each test");
    }
}
